/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.metadata;

import java.util.Collection;

/**
 * 分页接口
 *
 * @param <T> 分页数据类型
 */
public interface IPage<T>{
	/**
	 * 获取总页数
	 *
	 * @return 总页数
	 */
	int getTotal();

	/**
	 * 获取页面大小
	 *
	 * @return 页面大小
	 */
	int getPageSize();

	/**
	 * 获取当前页数
	 *
	 * @return 当前页数
	 */
	int getPage();
	
	
	/**
	 * 查询出的记录数
	 * 
	 * @return
	 */
	int getRecords();

	/**
	 * 是否为第一页
	 *
	 * @return 是否为第一页
	 */
	boolean isFirstPage();

	/**
	 * 是否为最后一页
	 *
	 * @return 是否为最后一页
	 */
	boolean isLastPage();

	/**
	 * 获取当前页起始记录行
	 *
	 * @return 起始记录行
	 */
	int getPageBegin();

	/**
	 * 获取当前页结束记录行
	 *
	 * @return 结束记录行
	 */
	int getPageEnd();

	/**
	 * 获取页面数据
	 *
	 * @return 页面数据
	 */
	Collection<T> getRows();
}
