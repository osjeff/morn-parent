/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.metadata;
public class GridModelFormatOptions {
	private String srcformat;
	private String newformat;
	private String suffix;

	public String getSrcformat() {
		return srcformat;
	}

	public void setSrcformat(String srcformat) {
		this.srcformat = srcformat;
	}

	public String getNewformat() {
		return newformat;
	}

	public void setNewformat(String newformat) {
		this.newformat = newformat;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
}
