/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.metadata;

import org.hibernate.type.Type;

public class ColumnType {
	String column;
	Type type;
	
	
	public ColumnType() {
	}

	public ColumnType(String column) {
		super();
		this.column = column;
	}

	public ColumnType(String column, Type type) {
		super();
		this.column = column;
		this.type = type;
	}
	
	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public boolean isNullType(){
		return this.type==null;
	}
	
	public boolean isNotNullType(){
		return this.type!=null;
	}
	
}
