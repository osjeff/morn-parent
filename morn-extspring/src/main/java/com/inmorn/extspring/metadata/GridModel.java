/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.metadata;

import java.util.Map;

public class GridModel {
	private String name;
	private String index;
	private String width;
	private String formatter;
	private GridModelEditOptions editoptions;
	private GridModelFormatOptions formatoptions;
	private Boolean hidden;
	private Boolean hidedlg;
	
	// generate property
	private Integer realWidth;
	private Map<String, String> maps;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getFormatter() {
		return formatter;
	}

	public void setFormatter(String formatter) {
		this.formatter = formatter;
	}

	public GridModelEditOptions getEditoptions() {
		return editoptions;
	}

	public void setEditoptions(GridModelEditOptions editoptions) {
		this.editoptions = editoptions;
	}

	public Boolean getHidden() {
		return hidden;
	}

	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}

	public Boolean getHidedlg() {
		return hidedlg;
	}

	public void setHidedlg(Boolean hidedlg) {
		this.hidedlg = hidedlg;
	}

	public Integer getRealWidth() {
		return realWidth;
	}

	public void setRealWidth(Integer realWidth) {
		this.realWidth = realWidth;
	}

	public Map<String, String> getMaps() {
		return maps;
	}

	public void setMaps(Map<String, String> maps) {
		this.maps = maps;
	}

	public GridModelFormatOptions getFormatoptions() {
		return formatoptions;
	}

	public void setFormatoptions(GridModelFormatOptions formatoptions) {
		this.formatoptions = formatoptions;
	}
}
