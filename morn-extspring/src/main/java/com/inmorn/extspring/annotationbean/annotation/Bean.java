/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.annotationbean.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Jeff.Li
 * @date 2017-07-13
 * 定义上下文bean
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Bean {

	/**
	 * singleton
	 */
	public static String BEAN_SCOPE_SINGLETON = "singleton";
	
	/**
	 * prototype
	 */
	public static String BEAN_SCOPE_PROTOTYPE = "prototype";
	
	/**
	 * request
	 */
	public static String BEAN_SCOPE_REQUEST = "request";
	
	/**
	 * session
	 */
	public static String BEAN_SCOPE_SESSION = "session";

	/**
	 * globalSession
	 */
	public static String BEAN_SCOPE_GLOBAL_SESSION = "globalSession";
	
	/**
	 * bean Id,定义唯一名(默认取定义属性名)
	 * @return
	 */
	String id() default "";
	
	/**
	 * bean对应的class
	 * @return
	 */
	String classes() default "";
	
	/**
	 * bean的作用域,默认单例
	 * @return
	 */
	String scope() default BEAN_SCOPE_SINGLETON;

	/**
	 * bean的父级bean名称
	 * @return
	 */
	String parent() default "";
	
	/**
	 * bean是否是抽象的
	 * @return
	 */
	boolean beanAbstract() default false;

	/**
	 * bean初始化方法名
	 * @return
	 */
	String initMethodName() default "";
	
	/**
	 * bean销毁方法名
	 * @return
	 */
	String destroyMethodName() default "";
	
	/**
	 * bean factoryMethod
	 * @return
	 */
	String factoryMethod() default "";
	
	/**
	 * 是否延迟初始化
	 * @return
	 */
	boolean lazyInit() default false;
	
	/**
	 * bean类中的属性
	 * @return
	 */
	Property[] propertys() default {};
	
	/**
	 * 备注
	 * @return
	 */
	String remark() default "";

}
