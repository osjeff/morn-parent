/**
 * Copyright 2016- the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inmorn.extspring.annotationbean.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Jeff.Li
 * @date 2017-10-31
 * MyBatis Mapper接口中使用
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyBatisMapper {
	
	/**
	 * 指定上下文中sqlSessionFactory,
	 * @return
	 */
	String sqlSessionFactory() default "";
	
	/**
	 * 指定上下文中sqlSessionTemplate,
	 * @return
	 */
	String sqlSessionTemplate() default "";
	
	/**
	 * addToConfig
	 * @return
	 */
	boolean addToConfig() default true;
	
}
