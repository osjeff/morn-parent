/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.annotationbean.support;

import java.lang.annotation.Annotation;
import java.util.Properties;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import com.inmorn.extspring.util.StringUtils;


/**
 * 自定义注解转换BeanDefinition抽象类
 * @author Jeff.Li
 * 2017-11-14
 */
public abstract class AbstractAnnotationProcessr{

	protected Properties properties;
	protected Environment environment;
	protected ApplicationContext applicationContext;
	
	/**
	 * 初始化参数
	 * @param applicationContext
	 * @param properties
	 * @param environment
	 */
	public void init(ApplicationContext applicationContext,
			Properties properties,Environment environment){
		this.applicationContext = applicationContext;
		this.properties = properties;
		this.environment = environment;
	}
	
	/**
	 * 获取属性文件值
	 * @param key
	 * @return
	 */
	public String getProperty(String key){
		String value = null;
		if(properties != null){
			value = properties.getProperty(key);
		}
		if(StringUtils.isBlank(value) && environment != null){
			value = environment.getProperty(key);
		}
		return value;
	}
	
	/**
	 * 获取自定义注解类
	 * @return
	 */
	public abstract Class<? extends Annotation> getAnnotation();
	
	/**
	 * 类转换为BeanDefinition
	 * @param beanClass
	 * @param annotation
	 * @return
	 */
	public abstract BeanDefinition classParseBeanDefinition(Class<?> beanClass,Annotation annotation);
	
}
