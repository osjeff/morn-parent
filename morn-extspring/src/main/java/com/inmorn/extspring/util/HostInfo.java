/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.util;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class HostInfo {

	public static Map<String, String> getHostInfo(){
		Map<String, String> result = new HashMap<String, String>();
		DecimalFormat df = new DecimalFormat("######0.00");
		Runtime run = Runtime.getRuntime();
		int mb = 1024 * 1024;
		result.put("totalMemory", df.format((run.totalMemory() / mb)) + "MB");
		result.put("freeMemory", df.format((run.freeMemory() / mb)) + "MB");
		result.put("maxMemory", df.format((run.maxMemory() / mb)) + "MB");
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println(getHostInfo());
	}
	
	
}