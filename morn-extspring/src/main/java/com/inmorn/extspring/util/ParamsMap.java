/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.util;

import java.util.HashMap;
import java.util.Map;

public class ParamsMap {

	
	public static Map<String, Object> params(String[] keys,Object[] args){
		if(keys == null || keys.length == 0
				|| args == null || args.length == 0 ||
				keys.length != args.length){
			return null;
		}
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		for (int i = 0; i < keys.length; i++) {
			paramsMap.put(keys[i],args[i]);
		}
		return paramsMap;
	}
	
	
	public static void main(String[] args) {
		Map<String, Object> map1 = 
				ParamsMap.params(new String[]{"code","name"},
					     		 new Object[]{"admin","管理员"});
		System.out.println(map1);
		
		Map<String, Object> map2 = 
				ParamsMap.params(new String[]{"beginTime","endTime"},
								 new Object[]{"2016-04-20","2016-04-27"});
		System.out.println(map2);
	}
	
	
}
