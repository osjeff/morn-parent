/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.quartz;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 配置JobBean任务类
 * @author Jeff.Li
 * 2016-12-16
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JobBean {
	
	/**
	 * 线程数可使用${}配置例:${test.job.threadNum}
	 * @return
	 */
	String thread() default "1";
	
	/**
	 * cron表达式可使用${}配置例:${test.job.corn}
	 * @return
	 */
	String cron() default "";
	
	/**
	 * Simple 指示间隔多少秒执行单位秒
	 * @return
	 */
	String repeatSecond() default "";
	
	/**
	 * Job描述
	 * @return
	 */
	String desc() default "";
	
	/**
	 * 参数1可使用${}
	 * @return
	 */
	String param1() default "";
	
	/**
	 * 参数2可使用${}
	 * @return
	 */
	String param2() default "";
	
}
