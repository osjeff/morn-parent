/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.quartz;

import java.util.Map;


public interface IScheduleService {
	
	/**
	 * 增加JOB
	 * @param className
	 * @param suffix
	 * @return
	 */
	public Map<String, Object> addJob(String className,String suffix);
	
	/**
	 * 创建JOB
	 * @param className
	 * @return
	 */
	public String createJob(String className ,boolean create)throws ClassNotFoundException;
	
	/**
	 * 启动JOB
	 * @param scheduleJob
	 * @param threadNum
	 * @param jobClass
	 * @return
	 */
	public boolean startJob(ScheduleJob scheduleJob,int threadNum,Class <? extends org.quartz.Job> jobClass);
	
	/**
	 * 暂停JOB
	 * @param jobKey
	 * @return
	 */
	public boolean pausseJob(String name,String group);
	
	/**
	 * 恢复JOB
	 * @param jobKey
	 * @return
	 */
	public boolean resumeJob(String name,String group);
	
	/**
	 * 移除JOB
	 * @param jobKey
	 * @return
	 */
	public boolean deleteJob(String name,String group);
	
	/**
	 * 获取JOB状态
	 * @return
	 */
	public  Map<String, Object> getJobState();
	
	/**
	 * 修改JOB参数
	 * @param param1
	 * @param param2
	 * @param name
	 * @param group
	 * @return
	 */
	public boolean updateJobParam(String param1,String param2,String name,String group);
	
}
