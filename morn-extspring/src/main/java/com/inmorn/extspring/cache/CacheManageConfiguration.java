/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CacheManageConfiguration {
	/**
	 * 所有可能需要接收缓存刷新的节点列表；
	 * key为名称，value为节点的接收JSON-RPC刷新缓存消息的访问地址；
	 * 比如：http://192.168.2.202:9094/test/refreshCache.action；
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map<String, String> endpoints = new HashMap();
	
	/**
	 * 针对不同缓存的关注列表，
	 * 其中，key为缓存的名称，value为节点名称的列表；
	 * 通过节点名称，在endpoints中可以找到对应的url；
	 */
	private Map<String, List<String>> cacheManagerPeers;
	
	/**
	 * http请求超时时间，
	 * 不能定义的过大，避免在服务不能用的时候阻塞用户操作；
	 * 单位为毫秒；
	 */
	private int connectionTimeout = 1000;
	
	/**
	 * socket超时时间；
	 * 不能定义的过大；避免在服务不能用的时候阻塞用户操作；
	 * 单位为毫秒；
	 */
	private int socketTimeout = 1000;
	
	/**
	 * JSON-RPC的服务方法名称，发出JSON-RPC调用时需要指定的接收端的服务方法名称；
	 */
	private String jsonRpcServiceMethodName;

	public Map<String, String> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(Map<String, String> endpoints) {
		this.endpoints = endpoints;
	}

	public Map<String, List<String>> getCacheManagerPeers() {
		return cacheManagerPeers;
	}

	public void setCacheManagerPeers(Map<String, List<String>> cacheManagerPeers) {
		this.cacheManagerPeers = cacheManagerPeers;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public String getJsonRpcServiceMethodName() {
		return jsonRpcServiceMethodName;
	}

	public void setJsonRpcServiceMethodName(String jsonRpcServiceMethodName) {
		this.jsonRpcServiceMethodName = jsonRpcServiceMethodName;
	}
}
