/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache.lru;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inmorn.extspring.cache.BaseEhCacheDataProvider;
import com.inmorn.extspring.cache.ICache;


public abstract class LRUBaseCacheDataProvider extends BaseEhCacheDataProvider {

	protected final Logger logger = LoggerFactory
			.getLogger(LRUBaseCacheDataProvider.class);

	protected ICache createCache() {
		if (initialCapacity <= 0)
			initialCapacity = 100;
		
		if (concurrencyLevel <= 0)
			concurrencyLevel = 100;
		
		LRUBaseCache cache = new LRUBaseCache(initialCapacity, concurrencyLevel);
		cache.setCacheName(cacheName);
		cache.setTtl(ttl * 60 * 1000);
		cache.setNeedLastAccessTime(needLastAccessTime);
		cache.setNullSetCache(nullSetCache);
		
		if (sleepTime <= 0)
			sleepTime = 5;
		
		cache.setSleepTime(sleepTime * 60 * 1000);

		if (threshHold <= 0)
			threshHold = 1000;
		
		if (maxSize < threshHold + 10)
			maxSize = 2 * threshHold;
		
		cache.setThreshHold(threshHold);
		cache.setMaxSize(maxSize);
		return cache;
	}

	/**
	 * 分钟数
	 */
	long ttl;

	/**
	 * @return the ttl
	 */
	public long getTtl() {
		return ttl;
	}

	/**
	 * @param ttl
	 *            the ttl to set
	 */
	public void setTtl(long ttl) {
		this.ttl = ttl;
	}

	/**
	 * 分钟数
	 */
	long sleepTime;

	/**
	 * @return the sleepTime
	 */
	public long getSleepTime() {
		return sleepTime;
	}

	/**
	 * @param sleepTime
	 *            the sleepTime to set
	 */
	public void setSleepTime(long sleepTime) {
		this.sleepTime = sleepTime;
	}

	int initialCapacity;
	int concurrencyLevel;
	int threshHold;
	int maxSize;
	
	/**
	 * 是否每次获取都设置最后访问时间
	 */
	public boolean needLastAccessTime = false;
	
	/**
	 * 是否将查询为null的放入缓存
	 * @return
	 */
	public boolean nullSetCache = false;
	

	public boolean isNullSetCache() {
		return nullSetCache;
	}

	public void setNullSetCache(boolean nullSetCache) {
		this.nullSetCache = nullSetCache;
	}

	public boolean isNeedLastAccessTime() {
		return needLastAccessTime;
	}

	public void setNeedLastAccessTime(boolean needLastAccessTime) {
		this.needLastAccessTime = needLastAccessTime;
	}

	/**
	 * @return the maxSize
	 */
	public int getMaxSize() {
		return maxSize;
	}

	/**
	 * @param maxSize
	 *            the maxSize to set
	 */
	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	/**
	 * @return the initialCapacity
	 */
	public int getInitialCapacity() {
		return initialCapacity;
	}

	/**
	 * @return the concurrencyLevel
	 */
	public int getConcurrencyLevel() {
		return concurrencyLevel;
	}

	/**
	 * @return the threshHold
	 */
	public int getThreshHold() {
		return threshHold;
	}

	/**
	 * @param initialCapacity
	 *            the initialCapacity to set
	 */
	public void setInitialCapacity(int initialCapacity) {
		this.initialCapacity = initialCapacity;
	}

	/**
	 * @param concurrencyLevel
	 *            the concurrencyLevel to set
	 */
	public void setConcurrencyLevel(int concurrencyLevel) {
		this.concurrencyLevel = concurrencyLevel;
	}

	/**
	 * @param threshHold
	 *            the threshHold to set
	 */
	public void setThreshHold(int threshHold) {
		this.threshHold = threshHold;
	}

}
