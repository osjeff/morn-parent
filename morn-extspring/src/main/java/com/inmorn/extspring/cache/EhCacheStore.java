/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class EhCacheStore implements ICacheStore {

	private final net.sf.ehcache.Cache cache;

	public EhCacheStore(String name) {
		final CacheManager manager = CacheManager.getInstance();
		Cache c = manager.getCache(name);
		if (c == null) {
			manager.addCache(name);
			c = manager.getCache(name);
		}
		this.cache = c;
		if (this.cache == null)
			throw new java.lang.IllegalStateException(
					"Can not create ehCache entity! Name[" + name + "]");
	}

	public Object getCacheData(Object key) {
		Element element = cache.get(key);
		if (element == null)
			return null;
		return element.getObjectValue();
	}

	public boolean isDataInCache(Object key) {
		return cache.isKeyInCache(key);
	}

	public void putCacheData(Object key, Object data) {
		cache.put(new Element(key, data));
	}

	public void shutdown() {
		cache.dispose();
	}

	public void removeCacheData(Object key) {
		if (cache != null) {
			cache.remove(key);
		}
	}

}
