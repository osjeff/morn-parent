/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache.ttl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inmorn.extspring.cache.BaseEhCacheDataProvider;
import com.inmorn.extspring.cache.ICache;



/**
 * 具体的缓存实现模块继承该类
 * 注意：一个CACHE只能支持一个KEY
 *
 */
public abstract class TTLBaseCacheDataProvider extends BaseEhCacheDataProvider {

	protected final Logger logger = LoggerFactory
			.getLogger(TTLBaseCacheDataProvider.class);

	@Override
	protected ICache createCache() {
		TTLBaseCache cache = new TTLBaseCache();
		cache.setCacheName(cacheName);
		cache.setTtl(ttl * 60 * 1000);
		return cache;
	}

	/**
	 * 分钟数
	 */
	long ttl;

	/**
	 * @return the ttl
	 */
	public long getTtl() {
		return ttl;
	}

	/**
	 * @param ttl
	 *            the ttl to set
	 */
	public void setTtl(long ttl) {
		this.ttl = ttl;
	}

}
