/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache.ttl;

import com.inmorn.extspring.cache.CacheEntry;



/**
 * 缓存实体,提供缓存创建时间,用于比对缓存是否已过期
 *
 */
public class TTLCacheEntry extends CacheEntry implements
	Comparable<TTLCacheEntry> {

	long	createTime	= System.currentTimeMillis ();

	public boolean isExpired( long ttl ) {
		if ( ttl < 0 )
			return false;
		return ( System.currentTimeMillis () - createTime ) > ttl;
	}

	/**
	 * @return the createTime
	 */
	public long getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime( long createTime ) {
		this.createTime = createTime;
	}

	public int compareTo(TTLCacheEntry o) {
		if (createTime > o.getCreateTime())
			return 1;
		if (createTime < o.getCreateTime())
			return -1;
		return 0;
	}
}
