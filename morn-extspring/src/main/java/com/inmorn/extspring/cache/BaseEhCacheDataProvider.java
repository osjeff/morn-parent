/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * CacheStore缺省使用ehcache,具体的实现可只继承该类,抽象类已经代理实现了缓存组件的初始化
 *
 */
public abstract class BaseEhCacheDataProvider implements ICacheDataProvider,
		InitializingBean, DisposableBean {

	protected final Logger logger = LoggerFactory
			.getLogger(BaseEhCacheDataProvider.class);

	protected String cacheName;

	/**
	 * @return the cacheName
	 */
	public String getCacheName() {
		return cacheName;
	}

	/**
	 * @param cacheName
	 *            the cacheName to set
	 */
	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}

	public void afterPropertiesSet() throws Exception {
		ICache cache = initializeCache();
		if (cache != null)
			CacheManager.getInstance().regCache(cache.getCacheName(), cache);
	}

	protected ICache cache = null;

	protected ICache initializeCache() {
		ICache cache = getCache();
		if (cache == null) {
			cache = this.createCache();
			cache.setCacheProvider(this);
			setCache(cache);
		}
		return cache;
	}

	/**
	 * 不同的子类可能使用的CACHE类型不同,子类覆盖该方法,缺省使用ehcache
	 */
	protected ICache createCache() {
		ICache cache = new BaseCache();
		cache.setCacheName(cacheName);
		EhCacheStore store = new EhCacheStore(cacheName);
		cache.setCacheStore(store);
		return cache;
	}

	/**
	 * @return the cache
	 */
	protected ICache getCache() {
		return cache;
	}

	/**
	 * @param cache
	 *            the cache to set
	 */
	protected void setCache(ICache cache) {
		this.cache = cache;
	}

	public void destroy() throws Exception {
		ICache cache = getCache();
		if (cache != null)
			cache.shutdown();
	}

}
