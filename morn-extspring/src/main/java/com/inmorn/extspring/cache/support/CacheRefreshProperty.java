/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache.support;

import java.util.List;

public class CacheRefreshProperty {
	private String cacheName;
	private List<String> cacheKeys;
	private boolean refreshed = false;
	
	public String getCacheName() {
		return cacheName;
	}
	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}
	public List<String> getCacheKeys() {
		return cacheKeys;
	}
	public void setCacheKeys(List<String> cacheKeys) {
		this.cacheKeys = cacheKeys;
	}
	public boolean isRefreshed() {
		return refreshed;
	}
	public void setRefreshed(boolean refreshed) {
		this.refreshed = refreshed;
	}
}
