/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache;

public class CacheEntry {
	//数据对象
	protected Object data;

	//键值
	protected Object key;

	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @return the key
	 */
	public Object getKey() {
		return key;
	}

	/**
	 * @param data the data to set
	 */
	public void setData( Object data ) {
		this.data = data;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey( Object key ) {
		this.key = key;
	}



}
