/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache;

import java.io.IOException;
import java.security.MessageDigest;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inmorn.extspring.util.StringUtils;

/**
 * 刷新本地cache缓存servlet
 * @author Jeff.Li
 * 2017-09-30
 */
public class CacheRefreshServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7058322945521956077L;
	protected final Logger LOG = LoggerFactory.getLogger(getClass());
	protected static String REFRESH_CACHE_KEY = "C4F8iNf8q6ThVOwIOXf633Uc";
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		String refreshCacheKey = this.getInitParameter("refreshCacheKey");
		if(StringUtils.isNotEmpty(refreshCacheKey)){
			REFRESH_CACHE_KEY = refreshCacheKey;
		}
	}
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException{
		doPost(req, resp);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		if("POST".equals(request.getMethod())){
			String cacheName = request.getParameter("cacheName");
			String cacheKey = request.getParameter("cacheKey");
			String sign = request.getParameter("sign");
			if(StringUtils.isEmpty(cacheName) || StringUtils.isEmpty(cacheKey) || StringUtils.isEmpty(sign)){
				LOG.info("cacheName or cacheKey or sign is null.");
				return;
			}
			String localSign = doSign(cacheName + cacheKey, "utf-8", REFRESH_CACHE_KEY);
			if(!localSign.equals(sign)){
				LOG.info("sign not equals, localSign:" + localSign + ",sign:" + sign);
				return;
			}
			
			ICache cache = CacheManager.getInstance().getCache(cacheName);
			if(cache != null){
				cache.removeData(cacheKey);
				LOG.info("本地缓存名称[" + cacheName + "]Key:[" + cacheKey + "]已刷新");
			}else{
				LOG.info("未找到本地缓存名称[" + cacheName + "]");
			}
		}
	}
	
	public static String doSign(String content, String charset, String keys) {
		String sign = "";
		content = content + keys;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(content.getBytes(charset));
			sign = new String(Base64.encodeBase64(md.digest()), charset);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return sign;
	}

}
