/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache.lru;

import com.inmorn.extspring.cache.CacheEntry;



/**
 * LRU 缓存对象，包含数据对象与最近访问时间
 * 
 */
public class LRUCacheEntry extends CacheEntry implements
		Comparable<LRUCacheEntry> {

	// 最近访问时间
	long lastAccessTime = System.currentTimeMillis();

	public long getLastAccessTime() {
		return lastAccessTime;
	}

	public void setLastAccessTime(long time) {
		lastAccessTime = time;
	}

	public boolean isExpired(long ttl) {
		// ttl为负数时永不过期
		if (ttl < 0)
			return false;

		return (System.currentTimeMillis() - lastAccessTime) > ttl;
	}

	public int compareTo(LRUCacheEntry o) {
		if (lastAccessTime > o.getLastAccessTime())
			return 1;
		if (lastAccessTime < o.getLastAccessTime())
			return -1;
		return 0;
	}

}
