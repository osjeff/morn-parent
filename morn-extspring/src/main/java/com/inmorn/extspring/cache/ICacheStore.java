/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.cache;

/**
 * 缓存数据存储,基于缓存组件操作数据
 *
 */
public interface ICacheStore {

	/**
	 * 通过键值获取缓存数据。
	 * @param key
	 * @return
	 */
	public Object getCacheData(Object key);

	/**
	 * 清除指定的缓存数据
	 * @param key
	 */
	public void removeCacheData(Object key);

	/**
	 * 将数据存到缓存存储。
	 * @param key
	 * @param data
	 */
	public void putCacheData(Object key,Object data);

	/**
	 * 非同步方法，通过键值判断数据是否在缓存存储中存在。
	 * @param key
	 * @return
	 */
	public boolean isDataInCache(Object key);

	/**
	 * 释放资源
	 */
	public void shutdown();
}
