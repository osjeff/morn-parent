/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.export.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Blank;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.biff.RowsExceededException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.inmorn.extspring.export.IExcelService;
import com.inmorn.extspring.metadata.ExportColType;
import com.inmorn.extspring.metadata.ExportSetting;
import com.inmorn.extspring.metadata.GridModel;
import com.inmorn.extspring.util.BeanUtils;
import com.inmorn.extspring.util.DateUtils;
import com.inmorn.extspring.util.RandomUtils;
import com.inmorn.extspring.util.StringUtils;

@Service
public class ExcelServiceImpl implements IExcelService {
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Value("${tmp.file.folder:/export/tmp}")
	private String tmpFileRoot;
	
	/**
	 * 从数据库读数据,生成带日期戳的Excel文件
	 * 
	 * @param result
	 *            数据结果列表
	 * @param setting
	 *            数据结果集对应Excel表列名映射导出对象
	 * @throws Exception
	 *             方法内的异常有ServiceException
	 */
	@SuppressWarnings("rawtypes")
	public void exportToFile(Collection result, ExportSetting setting) {
		Integer rdm = Math.abs(RandomUtils.getRandom().nextInt());
		Long timeMillis = System.currentTimeMillis();
		
		File dirname = new File(tmpFileRoot);
		if (!dirname.isDirectory())
		{ //目录不存在
		     dirname.mkdir(); //创建目录
		}  
		
		String fileName = tmpFileRoot + timeMillis + "_" + rdm + ".xls";
		
		try {
			File f = new File(fileName);
			f.createNewFile();
			export(new FileOutputStream(f), result, setting);
		}
		catch (IOException e) {
			if(logger.isDebugEnabled())logger.debug(fileName+"不存在, 请检查application.properties中的tmp.file.folder配置项, 保证在系统中存在相应的目录.");
		}
		
		// 设置真正的文件名
		setting.setRealName(fileName);
	}

	/**
	 * 从数据库读数据，写入Excel
	 * 
	 * @param os
	 *            数据流，如果是写本地文件的话，可以是FileOutputStream；
	 *            如果是写Web下载的话，可以是ServletOupputStream
	 * @param result
	 *            数据结果列表
	 * @param setting
	 *            数据结果集对应Excel表列名映射导出对象
	 * @throws Exception
	 *             方法内的异常有ServiceException
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	public void export(OutputStream os, Collection result, ExportSetting setting) {

		WritableWorkbook wbook = null;
		try {
			// 建立excel文件
			wbook = Workbook.createWorkbook(os);
			// sheet名称
			WritableSheet wsheet = wbook.createSheet("Sheet1", 0);
			
			DateFormat dateFormat = new DateFormat("yyyy-MM-dd hh:mm:ss");
			WritableCellFormat wcf = new WritableCellFormat(dateFormat);
			// 字体
			WritableFont wfont = null;
			// 字体格式
			WritableCellFormat wcfFC = null;
			// Excel表格的Cell
			Label wlabel = null;

			// 设置excel标题字体
			wfont = new WritableFont(WritableFont.ARIAL, 16, WritableFont.BOLD,
					false, jxl.format.UnderlineStyle.NO_UNDERLINE,
					jxl.format.Colour.BLACK);
			wcfFC = new WritableCellFormat(wfont);

			// 添加excel标题
			Label wlabel1 = new Label(5, 0, setting.getTitle(), wcfFC);
			wsheet.addCell(wlabel1);

			// 设置列名字体
			// 如果有标题的话，要设置一下偏移
			int offset = 2;
			if (setting.getTitle() == null
					|| setting.getTitle().trim().equals(""))
				offset = 0;
			else {
				wfont = new WritableFont(WritableFont.ARIAL, 14,
						WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
						Colour.BLACK);
				wcfFC = new WritableCellFormat(wfont);
			}

			// 根据ExportColNames来创建Excel的列名
			Map<Integer, String> colNames = setting.getExportColNames();
			int count = colNames.size();
			for (int i = 0; i < count; i++) {
//				String name = URLDecoder.decode(colNames.get(i),"utf-8");
				String name = colNames.get(i);
				if (name == null) {
					break;
				}
				wlabel = new Label(i, offset, name);
				wsheet.addCell(wlabel);
			}

			// 设置正文字体
			wfont = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD,
					false, jxl.format.UnderlineStyle.NO_UNDERLINE,
					jxl.format.Colour.BLACK);
			wcfFC = new WritableCellFormat(wfont);
			//設置字段類型用於設置
			WritableCell writableCell = null;
			// 往Excel输出数据
			int rowIndex = 1 + offset;
			//获得columnType
			Map colType = setting.getExportColTypes();
			String propertye = null;
			int row = 0;
			for (Object obj : result) {
				Map<Integer, String> colModels = setting.getExportColModels();
				Map<Integer, GridModel> colModelList = setting.getExportColModelList();
				int size = colModels.size();
				++row;
				wsheet.setColumnView(row,30);
				wsheet.setRowView(row,400);
				for (int i = 0; i < count; i++) {
					String col = colModels.get(i);
					GridModel model = colModelList.get(i);
					
					try {
						propertye = BeanUtils.getProperty(obj, col);
					} catch (Exception e) { 
						propertye = null;
					}
					
					if(propertye == null || "".equals(propertye)){
						writableCell = new Blank(i, rowIndex);
					} else if (model != null) {
						if (model.getEditoptions() != null) {	// map 转换
							String value = model.getMaps().get(propertye);
							writableCell = new Label(i, rowIndex, value);
						} else if (model.getFormatoptions() != null && 
								StringUtils.isNotEmpty(model.getFormatoptions().getNewformat())) {	// format date
							String format = model.getFormatoptions().getNewformat()
								.replaceAll("Y", "yyyy")
								.replaceAll("m", "MM")
								.replaceAll("d", "dd")
								.replaceAll("G", "HH")
								.replaceAll("i", "mm")
								.replaceAll("s", "ss")
								;
							Date date = new SimpleDateFormat(format).parse(propertye);
//							DateFormat dateFormat = new DateFormat(format);
//							WritableCellFormat wcf = new WritableCellFormat(dateFormat);
							writableCell = new DateTime(i, rowIndex, date, wcf);
						} else if (propertye != null) {
							writableCell = new Label(i, rowIndex, propertye);
						} else {
							writableCell = new Blank(i, rowIndex);
						}
					} else {
						ExportColType exportColType = (ExportColType)colType.get(col);
						if (exportColType == null) {
							writableCell = new Label(i, rowIndex, BeanUtils.getProperty(obj,
									col));
						}else {
							switch (exportColType) {
							case Boolean://boolean
								boolean flag = java.lang.Boolean.valueOf(propertye);
								writableCell = new jxl.write.Boolean(i, rowIndex,flag);
								break;
							case DateTime://日期
								Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(propertye);
								writableCell = new DateTime(i,rowIndex,date,wcf);
								break;
							case Label:	//文本
								writableCell = new Label(i, rowIndex, BeanUtils.getProperty(obj,
										col));
								break;
							case Number://數字
								Double double1  = Double.valueOf(propertye);
								writableCell = new jxl.write.Number(i, rowIndex, double1);
								break;
							case Formula://公式
								writableCell = new Formula(i,rowIndex,propertye);
								break;
							case Blank:	//空格
								writableCell = new Blank(i, rowIndex);
								break;
							default:
								writableCell = new Label(i, rowIndex, BeanUtils.getProperty(obj,
										col));
								break;
							}
						}
					}
					
					wsheet.addCell(writableCell);
					writableCell = null;
				}
				rowIndex++;
			}
			wbook.write(); // 写入文件
			os.flush();
		} catch (RowsExceededException e) {
			if(logger.isDebugEnabled())logger.debug("Excel行数超过65536行限制！");
		} catch (Exception e) {
			if(logger.isDebugEnabled())logger.debug("Excel文件操作失败！");
		} finally {
			try {
				if (wbook != null)
					wbook.close();
				os.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public void exportToFileBySheet(Collection result, ExportSetting setting) {
		Integer rdm = Math.abs(RandomUtils.getRandom().nextInt());
		Long timeMillis = System.currentTimeMillis();
		
		File dirname = new File(tmpFileRoot);
		if (!dirname.isDirectory())
		{ //目录不存在
		     dirname.mkdir(); //创建目录
		}  
		
		String fileName = tmpFileRoot + timeMillis + "_" + rdm + ".xls";
		
		try {
			File f = new File(fileName);
			f.createNewFile();
			exportBySheet(new FileOutputStream(f), result, setting);
		}
		catch (IOException e) {
			if(logger.isDebugEnabled())logger.debug(fileName+"不存在, 请检查application.properties中的tmp.file.folder配置项, 保证在系统中存在相应的目录.");
		}
		
		// 设置真正的文件名
		setting.setRealName(fileName);
	}
	
	@SuppressWarnings("rawtypes")
	public void exportByBigDate(Collection result, ExportSetting setting) {
		Integer rdm = Math.abs(RandomUtils.getRandom().nextInt());
		Long timeMillis = System.currentTimeMillis();
		
		File dirname = new File(tmpFileRoot);
		if (!dirname.isDirectory())
		{ //目录不存在
		     dirname.mkdir(); //创建目录
		}  
		
		String fileName = tmpFileRoot + timeMillis + "_" + rdm + ".xls";
		
		try {
			File f = new File(fileName);
			f.createNewFile();
			exportForBigData(new FileOutputStream(f), result, setting);
		}
		catch (IOException e) {
			if(logger.isDebugEnabled())logger.debug(fileName+"不存在, 请检查application.properties中的tmp.file.folder配置项, 保证在系统中存在相应的目录.");
		}
		
		// 设置真正的文件名
		setting.setRealName(fileName);
	}	
	/**
	 * 从数据库读数据，写入Excel
	 * 
	 * @param os
	 *            数据流，如果是写本地文件的话，可以是FileOutputStream；
	 *            如果是写Web下载的话，可以是ServletOupputStream
	 * @param result
	 *            数据结果列表
	 * @param setting
	 *            数据结果集对应Excel表列名映射导出对象
	 * @throws Exception
	 *             方法内的异常有ServiceException
	 */
	@SuppressWarnings({ "unused", "rawtypes" })
	public void exportBySheet(OutputStream os, Collection<?> result, ExportSetting setting) {

		WritableWorkbook wbook = null;
		int pageIndex = 1;
		try {
			// 建立excel文件
			wbook = Workbook.createWorkbook(os);
			// sheet名称
			WritableSheet wsheet = wbook.createSheet("Sheet"+pageIndex, 0);
			// 字体
			WritableFont wfont = null;
			// 字体格式
			WritableCellFormat wcfFC = null;
			// Excel表格的Cell
			Label wlabel = null;

			// 设置excel标题字体
			wfont = new WritableFont(WritableFont.ARIAL, 16, WritableFont.BOLD,
					false, jxl.format.UnderlineStyle.NO_UNDERLINE,
					jxl.format.Colour.BLACK);
			wcfFC = new WritableCellFormat(wfont);

			// 添加excel标题
			Label wlabel1 = new Label(5, 0, setting.getTitle(), wcfFC);
			wsheet.addCell(wlabel1);

			// 设置列名字体
			// 如果有标题的话，要设置一下偏移
			int offset = 2;
			if (setting.getTitle() == null
					|| setting.getTitle().trim().equals(""))
				offset = 0;
			else {
				wfont = new WritableFont(WritableFont.ARIAL, 14,
						WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
						Colour.BLACK);
				wcfFC = new WritableCellFormat(wfont);
			}

			// 根据ExportColNames来创建Excel的列名
			Map<Integer, String> colNames = setting.getExportColNames();
			int count = colNames.size();
			for (int i = 0; i < count; i++) {
//				String name = URLDecoder.decode(colNames.get(i),"utf-8");
				String name = colNames.get(i);
				if (name == null) {
					break;
				}
				wlabel = new Label(i, offset, name);
				wsheet.addCell(wlabel);
			}

			// 设置日期格式
			Map<String, WritableCellFormat> dateFormatter = new HashMap<String, WritableCellFormat>();
			String[] formatters = new String[]{"yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd H:mm:ss"};
			for (String formatter : formatters) {
				DateFormat dateFormat = new DateFormat(formatter);
				WritableCellFormat wcf = new WritableCellFormat(dateFormat);
				dateFormatter.put(formatter, wcf);
			}
			
			// 设置正文字体
			wfont = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD,
					false, jxl.format.UnderlineStyle.NO_UNDERLINE,
					jxl.format.Colour.BLACK);
			wcfFC = new WritableCellFormat(wfont);
			//設置字段類型用於設置
			WritableCell writableCell = null;
			// 往Excel输出数据
			int rowIndex = 1 + offset;
			//获得columnType
			Map colType = setting.getExportColTypes();
			String propertye = null;
			int row = 0;
			int rowCount = 0;
			for (Object obj : result) {
				Map<Integer, String> colModels = setting.getExportColModels();
				Map<Integer, GridModel> colModelList = setting.getExportColModelList();
				int size = colModels.size();
				++row;
				if(rowCount%60000==0 && rowCount!=0){
					pageIndex++;
					rowIndex = 1 + offset;
					row = 0;
					//创建新的一页
					wsheet = wbook.createSheet("Sheet"+pageIndex, 0);
					//创建标题
					for (int i = 0; i < count; i++) {
						String name = colNames.get(i);
						if (name == null) {
							break;
						}
						wlabel = new Label(i, offset, name);
						wsheet.addCell(wlabel);
					}
				}
				wsheet.setColumnView(row,30);
				wsheet.setRowView(row,400);
				for (int i = 0; i < count; i++) {
					String col = colModels.get(i);
					GridModel model = colModelList.get(i);
					
					try {
						propertye = BeanUtils.getProperty(obj, col);
					} catch (Exception e) { 
						propertye = null;
					}
					
					if(propertye == null || "".equals(propertye)){
						//writableCell = new Blank(i, rowIndex);
						if (model.getEditoptions() != null) {	// map 转换
							String value = model.getMaps().get("");
							writableCell = new Label(i, rowIndex, value);
						}
					} else if (model != null) {
						if (model.getEditoptions() != null) {	// map 转换
							String value = model.getMaps().get(propertye);
							writableCell = new Label(i, rowIndex, value);
						} else if (model.getFormatoptions() != null && 
								StringUtils.isNotEmpty(model.getFormatoptions().getNewformat())) {	// format date
							String format = model.getFormatoptions().getNewformat()
								.replaceAll("Y", "yyyy")
								.replaceAll("m", "MM")
								.replaceAll("d", "dd")
								.replaceAll("H", "HH")
								.replaceAll("G", "HH")
								.replaceAll("i", "mm")
								.replaceAll("s", "ss")
								;
							Date date = null;
							try {
								date = new SimpleDateFormat(format).parse(propertye);
							} catch (Exception e) {
								date = DateUtils.convert(propertye);
							}
//							DateFormat dateFormat = new DateFormat(format);
//							WritableCellFormat wcf = new WritableCellFormat(dateFormat);
//							writableCell = new DateTime(i, rowIndex, date, wcf);
							writableCell = new DateTime(i, rowIndex, date, dateFormatter.get(format));
							
						} else if (model.getFormatoptions() != null && StringUtils.isNotEmpty(model.getFormatoptions().getSuffix())) {
							String value = propertye.toString()+ model.getFormatoptions().getSuffix();
							writableCell = new Label(i, rowIndex, value);
						} else if (propertye != null) {
							writableCell = new Label(i, rowIndex, propertye);
						} else {
							writableCell = new Blank(i, rowIndex);
						}
					} 
					if(writableCell==null)
					{
					   writableCell = new Blank(i, rowIndex);
					}
					wsheet.addCell(writableCell);
					writableCell = null;
				}
				rowIndex++;
				rowCount++;
			}
			wbook.write(); // 写入文件
			os.flush();
		} catch (RowsExceededException e) {
			if(logger.isDebugEnabled())logger.debug("Excel行数超过65536行限制！");
		} catch (Exception e) {
			if(logger.isDebugEnabled())logger.debug("Excel文件操作失败！");
		} finally {
			try {
				if (wbook != null)
					wbook.close();
				os.close();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	
	
	
	@SuppressWarnings("rawtypes")
	public void exportForBigData(OutputStream os, Collection result, ExportSetting setting) {
//
//		// 创建新的Excel工作簿
//		SXSSFWorkbook book = new SXSSFWorkbook();
//		// 在Excel工作簿中建一工作表，其名为"Sheet1"
//		Sheet sheet1 = book.createSheet("Sheet1");
//
//		// 设置列名字体
//		// 如果有标题的话，要设置一下偏移
//		int offset = 2;
//		if (setting.getTitle() == null
//				|| setting.getTitle().trim().equals(""))
//			offset = 0;
//
//		Row rowz = sheet1.createRow(0);
////		// 根据ExportColNames来创建Excel的列名
//		Map<Integer, String> colNames = setting.getExportColNames();
//		int count = colNames.size();
//		for (int i = 0; i < count; i++) {
////			String name = URLDecoder.decode(colNames.get(i),"utf-8");
//			String name = colNames.get(i);
//			if (name == null) {
//				break;
//			}
//			rowz.createCell(i).setCellValue(name);
//		}
//
//		// 往Excel输出数据
//		int rowIndex = 1 + offset;
//		
//		int row = 1;
//		
//		String propertye = null;
//		for (Object obj : result) {
//			System.out.println("正在写入第"+row+"行。。。");
//			rowz = sheet1.createRow(row);
//			Map<Integer, String> colModels = setting.getExportColModels();
//			++row;
//			for (int i = 0; i < count; i++) {
//				String col = colModels.get(i);
//				
//				try {
//					propertye = BeanUtils.getProperty(obj, col);
//				} catch (Exception e) { 
//					propertye = null;
//				}
//				rowz.createCell(i).setCellValue(propertye);
//			}
//			rowIndex++;
//		}
//		
//		// 新建一输出文件流
//		// 把相应的Excel 工作簿存盘
//		try {
//			book.write(os);
//			os.flush();
//			// 操作结束，关闭文件
//			os.close();
//			System.out.println("文件生成...");
//		} catch (Exception e) {
//			System.out.println("已运行 xlCreate() : " + e);
//		}

	}

	public void sssss() {
		System.out.println("=="+tmpFileRoot);
	}
}