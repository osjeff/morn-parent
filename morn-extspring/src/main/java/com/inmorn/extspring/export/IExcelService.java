/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.export;

import java.io.OutputStream;
import java.util.Collection;

import com.inmorn.extspring.metadata.ExportSetting;

public interface IExcelService {

	
	/**
	  * 如果超过6W行自动分页
	  * @param result 数据结果列表
	  * @param setting 数据结果集对应Excel表列名映射导出对象
	  * @throws Exception 方法内的父类异常有SQLException和IOException
	  */
	 @SuppressWarnings("rawtypes")
	public void exportToFileBySheet(Collection result, ExportSetting setting);
	 
	/**
	  * 从数据库读数据,生成带日期戳的Excel文件
	  * @param result 数据结果列表
	  * @param setting 数据结果集对应Excel表列名映射导出对象
	  * @throws Exception 方法内的父类异常有SQLException和IOException
	  */
	 @SuppressWarnings("rawtypes")
	public void exportToFile(Collection result, ExportSetting setting);
	
	 /**
	  * 从数据库读数据，写入Excel
	  * @param os 数据流，如果是写本地文件的话，可以是FileOutputStream；
	  *    如果是写Web下载的话，可以是ServletOupputStream
	  * @param result 数据结果列表
	  * @param setting 数据结果集对应Excel表列名映射导出对象
	  * @throws Exception 方法内的父类异常有SQLException和IOException
	  */
	 @SuppressWarnings("rawtypes")
	public void export(OutputStream os, Collection result, ExportSetting setting);
	 
	 @SuppressWarnings("rawtypes")
	public void exportByBigDate(Collection result, ExportSetting setting);
	 
	 @SuppressWarnings("rawtypes")
	public void exportForBigData(OutputStream os, Collection result, ExportSetting setting);
	 
	public void sssss();
}