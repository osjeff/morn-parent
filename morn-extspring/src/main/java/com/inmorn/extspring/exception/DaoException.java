/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.exception;


public class DaoException extends BaseRuntimeException {
	private static final long serialVersionUID = 2138418113786442626L;

	/**
	 * 构造数据持久化操作异常
	 * 
	 * @param code
	 *            异常编号
	 */
	public DaoException(int code) {
		super(null, null, code, ExceptionSeverity.ERR_SEVERITY_NORMAL,
				ExceptionSource.ERR_SOURCE_DATABASE);
	}

	/**
	 * 构造数据持久化操作异常
	 * 
	 * @param message
	 *            原始异常信息
	 * @param code
	 *            异常编号
	 */
	public DaoException(String message, int code) {
		super(message, null, code, ExceptionSeverity.ERR_SEVERITY_NORMAL,
				ExceptionSource.ERR_SOURCE_DATABASE);
	}

	/**
	 * 构造数据持久化操作异常
	 * 
	 * @param cause
	 *            原始异常
	 * @param code
	 *            异常编号
	 */
	public DaoException(Throwable cause, int code) {
		super(null, cause, code, ExceptionSeverity.ERR_SEVERITY_NORMAL,
				ExceptionSource.ERR_SOURCE_DATABASE);
	}

	/**
	 * 构造数据持久化操作异常
	 * 
	 * @param code
	 *            异常编号
	 * @param severity
	 *            异常级别, 缺省为
	 */
	public DaoException(int code, int severity) {
		super(null, null, code, severity, ExceptionSource.ERR_SOURCE_DATABASE);
	}

	/**
	 * 构造数据持久化操作异常
	 * 
	 * @param message
	 *            原始异常信息
	 * @param code
	 *            异常编号
	 * @param severity
	 *            异常级别
	 */
	public DaoException(String message, int code, int severity) {
		super(message, null, code, severity,
				ExceptionSource.ERR_SOURCE_DATABASE);
	}

	/**
	 * 构造数据持久化操作异常
	 * 
	 * @param cause
	 *            原始异常
	 * @param code
	 *            异常编号
	 * @param severity
	 *            异常级别
	 */
	public DaoException(Throwable cause, int code, int severity) {
		super(null, cause, code, severity, ExceptionSource.ERR_SOURCE_DATABASE);
	}
}
