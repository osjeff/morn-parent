/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.exception;

/**
 * 错误来源
 * 
 *
 */
public interface ExceptionSource {

	public static int ERR_SOURCE_FRAMEWORK  = 0;
	public static int ERR_SOURCE_APPLICAITON_SERVER  = 1;
	public static int ERR_SOURCE_DATABASE  = 2;
	public static int ERR_SOURCE_DEPENDS_LIB  = 3;
	public static int ERR_SOURCE_APPLICAITON  = 4;
	public static int ERR_SOURCE_DEPLOY  = 5;
	public static int ERR_SOURCE_USER  = 6;
	public static int ERR_SOURCE_BIZ  = 7;
	public static int ERR_SOURCE_INTEGRATION  = 8;
	public static int ERR_SOURCE_PROCESS  = 9;
	public static int ERR_SOURCE_PROCESS_ENGINE  = 10;
	public static int ERR_SOURCE_OPERATE  = 11;
	public static int ERR_SOURCE_CONFIG  = 12;
	public static int ERR_SOURCE_SERVICE  = 13;
	public static int ERR_SOURCE_UNKNOWN  = 99;
}
