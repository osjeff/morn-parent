/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

public class BaseRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4364872269911149050L;

	/**
	 * 来源
	 */
	private int source;

	/**
	 * 严重程度
	 */
	private int severity;

	/**
	 * 代码
	 */
	private int code;

	// 错误参数
	private Object[] errorParameters;

	Throwable cause = null;

	public BaseRuntimeException(String message, Throwable cause) {
		this(message, cause, -1, ExceptionSource.ERR_SOURCE_UNKNOWN,
				ExceptionSeverity.ERR_SEVERITY_NORMAL);
	}

	public BaseRuntimeException(String message, Throwable cause, int code,
			int source, int severity) {
		super(message);
		this.cause = cause;
		this.code = code;
		this.severity = severity;
		this.source = source;
	}

	public BaseRuntimeException(String message, Throwable cause, int code) {
		this(message, cause, code, ExceptionSource.ERR_SOURCE_UNKNOWN,
				ExceptionSeverity.ERR_SEVERITY_NORMAL);
	}

	public BaseRuntimeException(String message) {
		super(message);
	}

	public BaseRuntimeException(Throwable cause) {
		super(cause.getMessage());
		this.cause = cause;
	}

	public BaseRuntimeException(int code) {
		this(null, null, code);
	}

	public Throwable getReason() {
		return cause;
	}

	public BaseRuntimeException() {
		super();
	}

	public void printStackTrace(PrintStream stream) {
		if (cause != null) {
			cause.printStackTrace(stream);
			stream.println(new StringBuffer("rethrow as exception ").append(
					this.getClass().getName()).toString());
		}
		super.printStackTrace(stream);
	}

	public void printStackTrace(PrintWriter writer) {
		if (cause != null) {
			cause.printStackTrace(writer);
			writer.println(new StringBuffer("rethrow as exception ").append(
					this.getClass().getName()).toString());
		}
		super.printStackTrace(writer);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public Object[] getErrorParameters() {
		return errorParameters;
	}

	public void setErrorParameters(Object[] errorParameters) {
		this.errorParameters = errorParameters;
	}
	

}
