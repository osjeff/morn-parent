/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.hessian;

/**
 * 令牌异常基类
 *
 *
 */
public class HessianException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * 构造令牌字符串格式化异常
	 */
	public HessianException() {
		super();
	}

	/**
	 * 构造令牌字符串格式化异常
	 *
	 * @param message 异常信息
	 * @param cause 异常信息
	 */
	public HessianException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 构造令牌字符串格式化异常
	 *
	 * @param cause 异常信息
	 */
	public HessianException(Throwable cause) {
		super(cause);
	}

	/**
	 * 构造令牌字符串格式化异常
	 *
	 * @param message 异常信息
	 */
	public HessianException(String message) {
		super(message);
	}

}
