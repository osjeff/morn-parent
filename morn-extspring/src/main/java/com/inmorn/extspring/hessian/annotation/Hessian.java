/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.hessian.annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * hessian注解类
 * @author Jeff.Li
 * 2016-01-22
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Hessian {
 
    String description() default "";
 
    boolean overloadEnabled() default false; // 是否支持方法重载
     
    /**
     * 两种方式:
     * 1.直接写远程服务的地址(uri="http://xxx/remote/xx")
     * 2.在属性文件中配置(xxx.url=http://xxx/remote/xx),
     * 	   在注解上直接写uri="${xxx.url}"
     * 3.为服务端时直接写请求地址后缀如("/hessianSoa","/track","/digest")
     * @return
     */
    String uri();
    
    /**
     * 是否为server端
     * @return
     */
    boolean server() default false;
    
    /**
     * soa接口实现类
     * 为server端时才使用
     * @return
     */
    Class<?> serverImpl() default Class.class;
 
}