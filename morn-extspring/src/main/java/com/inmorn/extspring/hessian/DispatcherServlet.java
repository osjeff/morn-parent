/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.hessian;

public class DispatcherServlet extends org.springframework.web.servlet.DispatcherServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8506567017876722867L;
	
	public String getContextConfigLocation() {
//		List<String> configs = ApplicationContext.getContext().getApplication().getBeansConfigs();
//		StringBuilder contextConfigLocations = new StringBuilder();
//		for(String config : configs) {
//			contextConfigLocations.append("classpath:").append(config).append(",");
//		}
		return super.getServletContext().getInitParameter("contextConfigLocation");
	}
}
