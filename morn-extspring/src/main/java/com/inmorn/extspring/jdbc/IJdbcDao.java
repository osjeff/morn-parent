/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.jdbc;

import java.util.List;
import java.util.Map;

import com.inmorn.extspring.exception.DaoException;

/**
 * JDBC数据操作接口, 用于高性能要求的查询与存储过程调用. 如:报表等
 * 
 *
 * @author 
 *
 */
public interface IJdbcDao{
	
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";
	public static final String ORDER_BY = " ORDER BY ";
	public static final String COMMA = " , ";
	public static final String SPACE = " ";
	public static final String COUNT = "COUNT__";
	
	/**
	 * SQL查询
	 *
	 * @param command SQL语句
	 * @param args 参数
	 * @return 数据集
	 * @throws JdbcDaoException JDBC出错时抛出
	 */
	List<Map<String, Object>> query(String command, List<Object> args) throws DaoException;

	/**
	 * 调用存储过程
	 *
	 * @param procedureName 存储过程名称
	 * @param args 参数
	 * @return 执行完成状态
	 * @throws JdbcDaoException JDBC出错时抛出
	 */
	void call(String procedureName, List<Object> args) throws DaoException;

	/**
	 * 调用查询存储过程
	 *
	 * @param procedureName 存储过程名称
	 * @param args 参数
	 * @return 数据集
	 * @throws JdbcDaoException JDBC出错时抛出
	 */
	List<Map<String, Object>> callQuery(String procedureName, List<Object> args) throws DaoException;

	/**
	 * 输出性调用存储过程
	 *
	 * @param procedureName 存储过程名称
	 * @param args 参数
	 * @param outs 输出参数 <输出参数索引号, 输出参数类型>, 参数类型如:Types.FLOAT
	 * @return 输出参数对应的数据 <输出参数索引号, 输出数据>
	 * @throws JdbcDaoException JDBC出错时抛出
	 */
//	Map<String, Object> callOut(String procedureName, List<Object> args) throws DaoException;
}
