/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.extspring.jdbc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * JdbcDao注解
 * @author Jeff.Li
 * 用于给dao实现层加数据源
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JdbcDao {

	/**
	 * 配置数据源bean的id
	 * 如果写成${dataSource} 则在属性文件中找KEY为dataSource对应的value
	 *    如果找到则用value作为数据源名去上下文中找数据源,
	 *    如果没找到则用${}中的值作为数据源名去找
	 * @return
	 */
	String dataSource();
	
}
