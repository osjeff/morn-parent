/**
 * Copyright 2016- the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inmorn.orm.mybatis.transaction;

import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.session.TransactionIsolationLevel;
import org.apache.ibatis.transaction.Transaction;
import org.apache.ibatis.transaction.TransactionFactory;

/**
 * @author Jeff.Li
 * @date 2016年11月9日
 * 注:扩展原mybatis-spring.jar
 */
public class MornManagedTransactionFactory implements TransactionFactory{

	public void setProperties(Properties props) {
		// TODO Auto-generated method stub
		
	}

	public Transaction newTransaction(Connection conn) {
		throw new UnsupportedOperationException("New Spring transactions require a DataSource");
	}

	public Transaction newTransaction(DataSource dataSource,
			TransactionIsolationLevel level, boolean autoCommit) {
		return new MornManagedTransaction(dataSource);
	}

}
