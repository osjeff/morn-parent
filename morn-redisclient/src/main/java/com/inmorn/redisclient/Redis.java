/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.redisclient;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Redis注解,使用在实现类的方法上
 * @author Jeff.Li
 * 2017-10-27
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Redis {

	/**
	 * sharded为需要使用到的redis集群
	 * 在application.properties文件中配置,例如："station.sharded"
	 * shardeds=station.sharded(shardeds=此属性必须有)
	 * station.sharded.hostPorts=10.129.220.28:7012(xxx.xxx.hostPorts=ip1:port1,ip2:port2...)多个节点以逗号隔开
	 * @return
	 */
	String sharded();
	
	/**
	 * 与sharded一样,如需使用多个redis集群则加上
	 * @return
	 */
	String shardedOther() default "";
}
