/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.redisclient;
import javassist.ClassMap;

public interface ICacheClient {

	/**
	 * 将单一string放到缓存
	 * @param key
	 * @param value
	 * @return 成功：返回OK
	 */
	public String setSingleString(String key, String value);
	
	/**
	 * 从缓存获取单一string
	 * @param key
	 * @param value
	 * @return 缓存的string
	 */
	public String getSingleString(String key);
	
	
	/**
	 * 将序列化对象插入缓存，暂不支持中文key（可能会有编码问题）
	 * @param key
	 * @param obj
	 * @return 成功：返回OK
	 */
	public String setSingleObject(String key, Object obj);
	
	/**
	 * 从缓存获取对象，暂不支持中文key（可能会有编码问题）
	 * @param key
	 * @param obj
	 * @return 缓存的对象
	 */
	public Object getSingleObject(String key);
	
	/**
	 * 获取对象，暂不支持中文key（可能会有编码问题）
	 * 
	 * @param key
	 * @param clazz
	 * @return
	 */
	public Object getSingleObject(String key, Class<?> clazz);
	
	/**
	 * 获取对象，暂不支持中文key（可能会有编码问题）
	 * 
	 * @param key
	 * @param clazz
	 * @return
	 */
	public Object getSingleObject(String key, Class<?> clazz, ClassMap classMap);
	
	/**
	 * 从缓存中删除一个key
	 * @param key
	 * @return 0：失败（key不存在） 
				1：成功
	 */
	public Long delKey(String key);
	
	/**
	 * 删除多个个keys
	 * @param keys
	 * @return
	 */
	public Long delKey(String... keys);
}
