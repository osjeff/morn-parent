/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.redisclient.redis;

import java.util.List;
import java.util.Map;

import com.inmorn.redisclient.ICacheClient;

import javassist.ClassMap;
import net.sf.json.JSONArray;
public interface IRedisClient extends ICacheClient{
	
	public List<?> getListTrim(String listKey,int number,Class<?> clazz);
	
	/**
	 * 如果缓存不存在这个key，则插入string到缓存，否则不做任何操作
	 * @param key
	 * @param value
	 * @return 1：成功
				0：失败
	 */
	public Long setNewString(String key, String value);
	
	/**
	 * 在缓存中的现有string末尾增加string
	 * @param key
	 * @param value
	 * @return 增加后的字符串长度
	 */
	public Long appendSingleString(String key, String value);
	/**
	 * 对缓存的数字value减少
	 * @param key
	 * @param num
	 * @return 减少后的数值
	 */
	public Long decrBy(String key, long num);

	/**
	 * 从缓存list中获取并删除对象
	 * @param key
	 * @param where 0 从头获取， 1从尾获取
	 * @return 
	 */
	public Object getAndRemoveObjectFromList(String key, int where);
	
	/**
	 * 从缓存list中获取并删除对象
	 * @param key
	 * @param where 0 从头获取， 1从尾获取
	 * @param obj
	 * @return 
	 */
	public Object getAndRemoveObjectFromList(String key, int where, Class<?> clazz);
	
	/**
	 * 从list中获取并删除对象
	 * @param key
	 * @param where 0 从头获取， 1从尾获取
	 * @param classMap
	 * @return
	 */
	public Object getAndRemoveObjectFromList(String key, int where, Class<?> clazz, ClassMap classMap);
	
	/**
	 * 从缓存list中获取并删除string
	 * @param key
	 * @param where 0 从头获取， 1从尾获取
	 * @return
	 */
	public String getAndRemoveStringFromList(String key, long where);
	
	/**
	 * 获取缓存list的长度
	 * @param key
	 * @return list长度
	 */
	public Long getListLen(String key);
	
	/**
	 * 根据index从缓存list中获取对象
	 * @param key
	 * @param index
	 * @return
	 */
	public Object getObjectByIndexFromList(String key, int index);
	
	/**
	 * 根据index从list中获取对象
	 * @param key
	 * @param index
	 * @param obj
	 * @return
	 */
	public Object getObjectByIndexFromList(String key, int index, Class<?> clazz);
	
	/**
	 * 根据index从list中获取对象
	 * @param key
	 * @param index
	 * @param obj
	 * @param classMap
	 * @return
	 */
	public Object getObjectByIndexFromList(String key, int index, Class<?> clazz, ClassMap classMap);
	
	/**
	 * 从缓存list中获取一个范围的object
	 * @param key
	 * @param beginIndex
	 * @param endIndex
	 * @param obj
	 * @return
	 */
	public List<?> getRangeObjectFromList(String key, int beginIndex,
			int endIndex, Class<?> clazz);
	
	/**
	 * 从list中获取一个范围的object
	 * 
	 * @param key
	 * @param beginIndex
	 * @param endIndex
	 * @param jsonConfig
	 * @return
	 */
	public List<?> getRangeObjectFromList(String key, int beginIndex,
			int endIndex, Class<?> clazz, ClassMap classMap);
	
	/**
	 * 获取缓存中的substring
	 * @param key
	 * @param beginIndex
	 * @param endIndex
	 * @return
	 */
	public String getRangeString(String key, long beginIndex, long endIndex);
	
	/**
	 * 从缓存list中获取一个范围的string
	 * @param key
	 * @param beginIndex
	 * @param endIndex
	 * @return
	 */
	public List<String> getRangeStringFromList(String key, long beginIndex,
			long endIndex);
	
	/**
	 * 根据index从缓存list中获取string
	 * 
	 * @param key
	 * @param index
	 * @return
	 */
	public String getStringByIndexFromList(String key, long index);
	
	/**
	 * 获取整个缓存Object list
	 * @param key
	 * @return
	 */
	public List<?> getWholeObjectList(String key, Class<?> clazz);
	
	/**
	 * 获取整个Object list
	 * @param key
	 * @param clazz
	 * @param jsonConfig
	 * @return
	 */
	public List<?> getWholeObjectList(String key, Class<?> clazz, ClassMap classMap);
	
	/**
	 * 获取整个缓存string list
	 * @param key
	 * @return
	 */
	public List<String> getWholeStringList(String key);
	
	/**
	 * 对缓存的数字value增加
	 * 
	 * @param key
	 * @param num
	 * @return 增加后的数值
	 */
	public Long incrBy(String key, long num);
	
	
	/**
	 * 在缓存list的object前/后插入一个object
	 * 
	 * @param key
	 * @param where 0前 1后
	 * @param pivot
	 * @param obj
	 * @return 无匹配：返回-1
				无key：返回0
				成功：返回list长度
	 */
	public Long insertObjectToList(String key, long where, String pivot,
			Object object);
	
	/**
	 * 在缓存list的string前/后插入一个string
	 * 
	 * @param key
	 * @param where 0前 1后
	 * @param pivot
	 * @param value
	 * @return 无匹配：返回-1
				无key：返回0
				成功：返回list长度
	 */
	public Long insertStringToList(String key, long where, String pivot,
			String value);
	
	/**
	 * 从缓存List里面移除object
	 * @param key
	 * @param count 移除次数
	 * @param obj
	 * @return 失败：返回0
				成功：返回删除的个数
	 */
	public Long remObjectFromList(String key, int count, Object obj);
	
	/**
	 * 从缓存List里面移除string
	 * 
	 * @param key
	 * @param count 移除次数
	 * @param value
	 * @return	失败：返回0
				成功：返回删除的个数
	 */
	public Long remStringFromList(String key, long count, String value);
	
	/**
	 * 在缓存list的index设置对象
	 * 
	 * @param key
	 * @param index
	 * @param obj
	 * @return 无效index：返回null
				成功：返回OK
	 */
	public String setObjectByIndexToList(String key, int index, Object obj);
	
	/**
	 * 将对象插入到已经存在的缓存list，暂不支持中文key（可能会有编码问题）
	 * 
	 * @param key
	 * @param obj
	 * @param where 0放到第一位 1放到最后一位
	 * @return 失败：返回0
				成功：返回list长度
	 */
	public Long setObjectToExistList(String key, Object obj, long where);
	
	/**
	 * 将对象插入到缓存list，暂不支持中文key（可能会有编码问题）
	 * 
	 * @param key
	 * @param obj
	 * @param where 0放到第一位 1放到最后一位
	 * @return list长度
	 */
	public Long setObjectToList(String key, Object obj, long where);
	
	
	/**
	 * 从缓存起始范围插入string
	 * 
	 * @param key
	 * @param beginIndex
	 * @param value
	 * @return 新字符串的长度
	 */
	public Long setRangeString(String key, long beginIndex, String value);
	
	/**
	 * 在缓存list的index设置string
	 * 
	 * @param key
	 * @param index
	 * @param value
	 * @return 无效index：返回null
				成功：返回OK
	 */
	public String setStringByIndexToList(String key, long index, String value);
	
	/**
	 * 将String插入到已经存在的缓存list
	 * 
	 * @param key
	 * @param value
	 * @param where
	 *            0放到第一位 1放到最后一位
	 * @return 失败：返回0
				成功：返回list长度
	 */
	public Long setStringToExistList(String key, String value, long where);
	
	/**
	 * 将string放到缓存list中
	 * 
	 * @param key
	 * @param value
	 * @param where
	 *            0放到第一位 1放到最后一位
	 * @return 失败：返回0
				成功：返回list长度
	 */
	public Long setStringToList(String key, String value, long where);
	
	/**
	 * 清除缓存list，只保留beginIndex和endIndex之间的内容
	 * 
	 * @param key
	 * @param beginIndex
	 * @param endIndex
	 * @return OK
	 */
	public String trimList(String key, long beginIndex, long endIndex);
	
	/**
	 * 批量获取key-value
	 * @param keys
	 * @return
	 */
	public Map<String, JSONArray> batchGetMap(String... keys);
	
	/**
	 * <b>模糊查询所有匹配的key</b><br/>
	 * key模糊规则如下:<br/>
	 * key="*string"查询以string结尾的key<br/>
	 * key="string*"查询以string开头的key<br/>
	 * key="*string*"查询存在string的key<br/>
	 * key="string*string"查询以string为开头和结尾的key<br/>
	 * @param key
	 * @return List<String> keys
	 */
	public List<String> getKeys(String key);
	
	/**
	 * 生产消费者模式专用，从生产者获得list
	 * @param key		生产者列表key
	 * @param count		每次取的list大小
	 * @return
	 */
	public List<String> getListFromProducer(String key, int count);
	
	/**
	 * 将List中string放到缓存list中
	 * 
	 * @param key
	 * @param list
	 * @param where
	 *            0放到第一位 1放到最后一位
	 * @return 失败：返回0
				成功：返回list长度
	 */
	public Long setStringToList(String key, List<String> list, long where);
	
	/**
	 * 保存list和String, 订单线下订单接入生产消费者模式专用
	 * @param key
	 * @param listKeyString
	 * @param value
	 * @param where
	 * @return
	 */
	public String setStringAndList(String key, String listKeyString, String value, long where );
	
	/**
	 * 通过keys返回LIST
	 * ADD BY HUANGJIANHUA 2013-09-27
	 * @param keys
	 * @return
	 */
	public List<String> getListBykeys(String... keys);
	/**
	 * 对list在一个事物中删除并增加string
	 * @param keyToRem
	 * @param strToRem
	 * @param keyToSet
	 * @param objToSet
	 * @param where
	 * @return
	 */
	public Long remAndSetStringToList(String keyToRem, String strToRem, String keyToSet, Object objToSet, int where);
	/**
	 * 添加对象的同时添加对应的关系
	 * @param objKey
	 * @param obj
	 * @param key1
	 * @param keyValue1
	 * @param key2
	 * @param keyValue2
	 * @return
	 */
	public Long addObjectAndStringToList(String objKey,Object obj,String key1,String keyValue1,String key2,String keyValue2,int where);
	/**
	 * 删除对象的同时删除对应的关系
	 * @param key
	 * @param keyValue
	 * @param key1
	 * @param keyValue1
	 * @param key2
	 * @param keyValue2
	 * @return
	 */
	public Long remObjectAndStringToList(String key,String keyValue,String key1,String keyValue1,String key2,String keyValue2);
	
	/**
	 * 随机返回count条散key-value
	 * @param listKey
	 * @param count
	 * @return
	 */
	public Map<String,String> getKeysByRang(String listKey, int count);
	
	/**
	 * 设置redis过期时间
	 * @param key
	 * @return
	 */
	public Long setExpire(String key);
	
	/**
	 * 移除List中的指定指定元素 add by shaoyi
	 * LREM key count value
	 *根据参数 count 的值，移除列表中与参数 value 相等的元素。
	 *count 的值可以是以下几种：
	 *•count > 0 : 从表头开始向表尾搜索，移除与 value 相等的元素，数量为 count 。
	 *•count < 0 : 从表尾开始向表头搜索，移除与 value 相等的元素，数量为 count 的绝对值。
	 *•count = 0 : 移除表中所有与 value 相等的值。
	 * @param key
	 * @param value
	 * @return
	 */
	public Long removeLrem(String key,long count,String s);
	
	/**
	 * add by mkd 2014/09/16 在同一管道中push数据到队列并保持散KEY
	 * @param listKey 队列KEY
	 * @param listValue 队列值
	 * @param stringMap 散Key及value Map
	 * @return 返回push到队列中成功下标及散key状态
	 * @throws Exception
	 */
	public List<Object> rpushListAndSetKey(String listKey, String key, String value) throws Exception ;
	/**
	 * rpush队列并setkey
	 * add by shaoyi
	 * @param listKey
	 * @param listValue
	 * @param stringMap
	 * @return
	 * @throws Exception 
	 */
	public List<Object> rpushListAndSetKey(String listKey,List<String> listValue,Map<String,String> stringMap) throws Exception;
	
	/**
	 * 通过Key列表获取知道数量的单号
	 * 获取成功后删除key
	 * @param listkey
	 * @param size 数量
	 * @return List<String>
	 * @throws Exception
	 */
	public List<String> getServiceObjectRemoveList(List<String> listkey, int size) throws Exception;
	/**
	 * 
	 * @param listKey
	 * @param key
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public Long rpushListValue(String listKey, String value) throws Exception;
	
	/***
	 * 使用Pipeline获取队列key,value,并删除 Add xielongfei  2016.01.12
	 * @param listKey
	 * @param size
	 * @return
	 */
	public Map<String, Object> getValueAndRemoveKeys(String listKey, int size);

}
