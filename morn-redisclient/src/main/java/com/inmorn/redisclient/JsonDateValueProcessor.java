/**
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inmorn.redisclient;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import com.inmorn.extspring.util.DateUtils;

public class JsonDateValueProcessor implements JsonValueProcessor {
	public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
	private String dateFormat = DEFAULT_DATE_PATTERN;

	public JsonDateValueProcessor() {

	}

	public JsonDateValueProcessor(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public Object processArrayValue(Object value, JsonConfig jsonConfig) {
		if (value == null)
			return null;

		String[] array = (String[]) value;
		Date[] dates = new Date[array.length];
		for (int i = 0, n = dates.length; i < n; i++) {
			dates[i] = DateUtils.convert(array[i]);
		}
		return dates;
	}

	public Object processObjectValue(String key, Object value,
			JsonConfig jsonConfig) {
		if (value == null)
			return null;

		return new SimpleDateFormat(dateFormat).format(value);
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

}