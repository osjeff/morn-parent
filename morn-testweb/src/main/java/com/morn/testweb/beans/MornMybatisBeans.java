/**
 * Copyright 2016- the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.morn.testweb.beans;

import com.inmorn.context.beans.annotation.Bean;
import com.inmorn.context.beans.annotation.Beans;
import com.inmorn.context.beans.annotation.Property;
import com.inmorn.orm.mybatis.MapperScannerConfigurer;
import com.inmorn.orm.mybatis.SqlSessionFactoryBean;

@Beans
public class MornMybatisBeans {

	@Bean(propertys={@Property(name="dataSource",ref="core_oracle_ds_rw")})
	SqlSessionFactoryBean sqlSessionFactory;
	
	@Bean(propertys={@Property(name="dataSource",ref="mysql_dataSource"),
 		     		 @Property(name="mapperLocations",value="classpath:com/morn/testweb/*/mapping/*.xml")})
	SqlSessionFactoryBean mysql_sqlSessionFactory;
	
	@Bean
	MapperScannerConfigurer mapperScannerConfigurer;
	
}
